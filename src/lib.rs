use std::io::Read;
use nom::{
    IResult,
    character::{is_alphabetic, streaming::line_ending},
    combinator::{map, map_res, opt},
    sequence::{delimited, terminated, tuple},
    bytes::streaming::{is_a, is_not, take_while1},
};

#[derive(Clone, Debug)]
pub enum Token {
    BraceContents(String),
    Field(String),
    Value(String),
}

pub struct Entry<T> {
    key: T,
    locale: Option<T>,
    value: T,
}

impl Entry<&str> {
    fn all_to_string(self) -> Entry<String> {
        Entry {
            key: self.key.to_string(),
            locale: self.locale.map(str::to_string),
            value: self.value.to_string(),
        }
    }
}

#[derive(Clone, Debug)]
pub enum Error {
    MissingGrouping,
    MissingDesktopEntryGroup,
    InvalidUtf8,
}

fn utf8_value(input: &[u8]) -> Result<&str, Error> {
    std::str::from_utf8(input).or(Err(Error::InvalidUtf8))
}

fn map_entry<'a>((key, locale, _, value): (&'a str, Option<&'a str>, &'a [u8], &'a str)) -> Entry<&'a str> {
    Entry { key, locale, value }
}

fn brace_contents(input: &[u8]) -> IResult<&[u8], &str> {
    map_res(
        delimited(is_a("["), is_not("]"), is_a("]")),
        utf8_value
    )(input)
}

fn group_line(input: &[u8]) -> IResult<&[u8], &str> {
    terminated(brace_contents, line_ending)(input)
}

fn entry_key_value(input: &[u8]) -> IResult<&[u8], Entry<&str>> {
    map(
        tuple((
            entry_key,
            opt(brace_contents),
            entry_separator,
            entry_value,
        )),
        map_entry
    )(input)
}

fn entry_key(input: &[u8]) -> IResult<&[u8], &str> {
    map_res(
        take_while1(is_alphabetic),
        utf8_value
    )(input)
}

fn entry_separator(input: &[u8]) -> IResult<&[u8], &[u8]> {
    take_while1(|c| c == b'=' || c == b' ' || c == b'\t')(input)
}

fn entry_value(input: &[u8]) -> IResult<&[u8], &str> {
    map_res(
        terminated(is_not("\r\n"), line_ending),
        utf8_value
    )(input)
}

pub struct Tokenizer<R: Read> {
    reader: R,
}

impl<R: Read> Tokenizer<R> {
    pub fn new(reader: R) -> Self {
        Self {
            reader
        }
    }
}

impl<R: Read> Iterator for Tokenizer<R> {
    type Item = Result<Token, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        None
    }
}

#[cfg(test)]
mod tests {
    use std::io::Cursor;
    use super::*;

    const EMPTY_FILE: &[u8] = b"";

    #[test]
    fn empty_file_errors() {
        let reader = Cursor::new(EMPTY_FILE);
        let mut tokenizer = Tokenizer::new(reader);
        matches!(tokenizer.next(), None);
    }
}
